package INF101.exam;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Collections;

class SreadOutTest {

	@Test
	void testSpread() {
		ArrayList<Integer> list = toList(1,2,3,4,5,10);
		ArrayList<Integer> found = SpreadOut.spread(4,list);
		assertEquals(4, found.size());
		assertTrue(found.contains(1));
		assertTrue(found.contains(3));
		assertTrue(found.contains(5));
		assertTrue(found.contains(10));
	}
	
	static ArrayList<Integer> toList(int... a){
		ArrayList<Integer> list = new ArrayList<>();
		for(int i:a) {
			list.add(i);
		}
		return list;
	}

	@Test
	void testSpreadNotSorted() {
		ArrayList<Integer> list = toList(1,5,2,7,4,11);
		ArrayList<Integer> found = SpreadOut.spread(4,list);
		assertEquals(4, found.size());
		assertEquals(3, findMinGap(found));
	}
	
	
	static int findMinGap(ArrayList<Integer> numbers) {
		int n = numbers.size();
		if(n<2)
			throw new IllegalArgumentException("There must be at least 2 numbers in the list");
		
		Collections.sort(numbers);
		int minGap = numbers.get(1)-numbers.get(0);
		
		for(int i=1; i<n; i++) {
			minGap = Math.min(minGap, numbers.get(i)-numbers.get(i-1));
		}
		
		return minGap;
	}
	
	@Test
	void testReturnsAll() {
		ArrayList<Integer> list = toList(1,2,3);
		ArrayList<Integer> ans = SpreadOut.spread(list.size(),list);
		for(int i : list) {
			assertTrue(ans.contains(i),"Answer does not contain "+i);
		}
	}

	@Test
	void testReturnsEnds() {
		ArrayList<Integer> list = toList(1,2,3,7,11);
		ArrayList<Integer> ans = SpreadOut.spread(2,list);
		assertEquals(2, ans.size());
		assertTrue(ans.contains(Collections.min(list)));
		assertTrue(ans.contains(Collections.max(list)));
	}
}