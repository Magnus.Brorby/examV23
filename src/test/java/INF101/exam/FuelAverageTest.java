package INF101.exam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FuelAverageTest {

	IAverage fa;
	int k;

	@BeforeEach
	void init() {
		k=10;
		fa = getInstance(k);
	}
	
	IAverage getInstance(int k) {
		return new FuelAverage(k);
	}
	
	@Test
	void testCanNotConstructWithZero() {
		assertThrows(IllegalArgumentException.class, () -> getInstance(0));
	}
	@Test
	void testCanNotConstructWithNegative() {
		assertThrows(IllegalArgumentException.class, () -> getInstance(-1));
	}
	
	@Test
	void testNoElementAdded() {
		assertThrows(IllegalStateException.class, () -> fa.average(), "Calling average before any elements added should cause IllegalStateException to be thrown");
	}
	
	@Test
	void testSingleElementAdded() {
		fa.add(3);
		assertEquals(3.0, fa.average(),0.0001,"Average is not computed correctly with 1 element.");
	}

	@Test
	void testSingleElementAdded2() {
		fa.add(7);
		assertEquals(7, fa.average(),0.0001,"Average is not computed correctly with 1 element.");
	}

	@Test
	void testRounding() {
		fa.add(7);
		assertEquals(7, fa.average(),0.0001,"Average is not computed correctly with 1 element.");
		fa.add(8);
		assertEquals(7.5, fa.average(),0.0001,"Average is not computed correctly with 2 elements.");
	}

	@Test
	void testAddSeries() {
		fa.add(7);
		assertEquals(7, fa.average(),0.0001,"Average is not computed correctly with 1 element.");
		fa.add(5);
		assertEquals(6, fa.average(),0.0001,"Average is not computed correctly with 2 elements.");
		fa.add(9);
		assertEquals(7, fa.average(),0.0001,"Average is not computed correctly with 3 elements.");
	}

	@Test
	void testAddLongSeries() {
		for(int i=1; i<=100; i++) {
			fa.add(i);
			if(i<=k)
				assertEquals((i+1)/2.0, fa.average(),0.0001,"Average is not computed correctly in the first k elements.");
			else
				assertEquals((2*i-k+1)/2.0, fa.average(),0.0001,"Average of consecutice numbers from "+(i-k+1)+" to "+i+" is wrong.");
		}
	}
	
	// This test checks that the FuelAverage can run for a long time without crashing
	// It is commented out because it takes a long time but can be commented in for a performance test.

//	@Test
//	void canRunForLongTime() {
//		for(int i=0; i<1000000000; i++)
//			fa.add(1000);
//		assertEquals(1000,fa.average(),0.0001);
//	}
}
